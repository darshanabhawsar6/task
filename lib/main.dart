import 'dart:async';

import 'package:flutter/material.dart';
import 'package:nalliance_task/model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Home(),
    );
  }
}

class Home extends StatelessWidget {
  List <Model> model=[
   Model("Take the mic for 10 mins","+10",Icons.mic),
   Model("Send gifts in the room","+6",Icons.card_giftcard_sharp),
   Model("Share rooms to social media platforms","+1",Icons.screen_share_outlined),
  ];
  List <Model> model1=[
    Model("Create Room","+10",Icons.home),
    Model("Bind phone number","+6",Icons.phone_android_outlined),
    Model("Get 10 likes for posts","+5",Icons.thumb_up),
    Model("Send 10 gifts to posts","+20",Icons.wallet_giftcard_sharp),
    Model("Add 10 friends","+5",Icons.people),
    Model("Stay in the room","+10",Icons.access_time_outlined),
    Model("Have 10 room members","+10",Icons.person_outline),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Task'),
        backgroundColor: Colors.greenAccent,
      ),
      body: Padding(
        padding: const EdgeInsets.only(left:8.0,right: 8,top: 5),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                //   height: 160,
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.yellow
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 18.0,bottom: 18),
                      child: Text("Sign In",style: TextStyle(fontSize: 24),),
                    ),

                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top:15.0,left: 8),
                child: Text("Daily Tasks",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
              ),
              taskList(context),
              Padding(
                padding: const EdgeInsets.only(top:15.0,left: 8),
                child: Text("Improvement Tasks",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
              ),
              iTaskList(context),
              Padding(
                padding: const EdgeInsets.only(top:20.0,bottom: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.storefront_outlined,color: Colors.lightBlue,),
                    SizedBox(width: 4,),
                    Text("Crystal Store",style: TextStyle(
                        color: Colors.lightBlue,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),),
                    Icon(Icons.keyboard_arrow_right_sharp,color: Colors.lightBlue,)
                  ],
                ),
              )
              
            ],
          ),
        ),
      )
    );
  }
  taskList(context){
    return ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: model.length,
        itemBuilder: (BuildContext cxt,int index){
          return  Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Stack(
                      children: [
                        Icon(Icons.circle,color: Colors.greenAccent,size: 50,),
                        Positioned(
                            left: 11,top: 13,
                            child: Icon(model[index].icon,color: Colors.white,))
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(model[index].title,style: TextStyle(
                          fontSize: 16,fontWeight: FontWeight.bold
                        ),),
                        SizedBox(height: 5,),
                        Row(
                          children: [
                            Image.asset("assets/images.png",height: 20,width: 20,),
                            Text(model[index].subtitle,style: TextStyle(
                              fontSize: 14,fontWeight: FontWeight.bold,color: Colors.blue
                            ),),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right:5.0),
                    child: Container(
                      width: 80,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                         border: Border.all(color: Colors.greenAccent)
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0,bottom: 5),
                            child: Text("Go",style: TextStyle(fontSize: 12,color: Colors.greenAccent),),
                          ),

                        ],
                      ),
                    ),
                  ),

                ],
              ),
              SizedBox(height: 5,),
              Container(
                height: 1.0,color: Colors.grey,width: MediaQuery.of(context).size.width/1.3,
              ),
              SizedBox(height: 5,),
            ],

          );
        });

  }
  iTaskList(context){
    return ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: model1.length,
        itemBuilder: (BuildContext cxt,int index){
          return  Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Stack(
                      children: [
                        Icon(Icons.circle,color: Colors.lightBlueAccent,size: 50,),
                        Positioned(
                            left: 11,top: 13,
                            child: Icon(model1[index].icon,color: Colors.white,))
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(model1[index].title,style: TextStyle(
                            fontSize: 16,fontWeight: FontWeight.bold
                        ),),
                        SizedBox(height: 5,),
                        Row(
                          children: [
                            Image.asset("assets/images.png",height: 20,width: 20,),
                            Text(model1[index].subtitle,style: TextStyle(
                                fontSize: 14,fontWeight: FontWeight.bold,color: Colors.blue
                            ),),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right:5.0),
                    child: Container(
                      width: 80,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          border: Border.all(color: Colors.greenAccent)
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0,bottom: 5),
                            child: Text("Go",style: TextStyle(fontSize: 12,color: Colors.greenAccent),),
                          ),

                        ],
                      ),
                    ),
                  ),

                ],
              ),
              SizedBox(height: 5,),
              Container(
                height: 1.0,color: Colors.grey,width: MediaQuery.of(context).size.width/1.3,
              ),
              SizedBox(height: 5,),
            ],

          );
        });

  }
}

